B

```
{"id":1232535235,"vote_id":2}
```

k_sec

```
p5lVdWxIDExI208MumcPhyyf3mAWTG6kva8E6qKvl10Rg/4G5Sck5OhIPJ1/+LZl
xx1dcmzaPxX+N/hzTp1xmERzHM8n4K/jeZUF8xDQXFQgN6/NBMQCMpCzuVpOu1hk
h2ME/yr5NqFY0B2uGGynjBzm0h2lAJIPgG9M2kdqKD4=
```

B_en = encrypt(k_sec, B);

```
kzkfRa9Ih8JwBYmaxe41CjCOTg2xoBsgGt6WFcM=
```

_skip blind_

h(B_bl) = md5(B_en)

```
ad1443fc4cb35dab515a666ae57f2a9e
```

DSи.bl = sign(k_и.priv, h(B_bl));

```
o8O2UrOi0PhXOyFoc4lI89IjV6JJuqdcf3BG9R7adhQvwdvmCOcb9t5qlLuFa1/KAbLUNIqNvpyq
DBU1v9gHJQ==
```

Check h(B_bl) == unsign(k_и.pub, DS_и.bl)

```
ad1443fc4cb35dab515a666ae57f2a9e
```

DS_р.bl = sign(k_р.priv, h(B_bl))

```
xq2PDydn1jC8kYPNS3IwuvOoaoUBvhiEijF2QWn+3kiZyBW2xHbjB+ZVFR2wnloq3SVqgLaG/11F
fa31MDfKNg==
```

_skip unblind_

B = decrypt(k_sec, B_en)

```
{"id":1232535235,"vote_id":2}
```
