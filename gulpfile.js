const gulp              = require('gulp')
const gulpLoadPlugins   = require('gulp-load-plugins')
const babel             = require('gulp-babel')
const browserify        = require('browserify')
const buffer            = require('gulp-buffer')
const log               = require('gulplog')
const tap               = require('gulp-tap')

const $ = gulpLoadPlugins()
let dev = true
const inputDir = './resources/assets'
const outputDir = './public'

// JS
gulp.task('js', function () {
    return gulp.src(`${inputDir}/js/*.js`, {read: false}) // no need of reading file because browserify does
        .pipe($.plumber())
        // transform file objects using gulp-tap plugin
        .pipe(tap(function (file) {
            log.info('Bundling ' + file.path)
            // replace file contents with browserify's bundle stream
            file.contents = browserify(file.path, {debug: true}).bundle()
        }))
        // transform streaming contents into buffer contents (because gulp-sourcemaps does not support streaming contents)
        .pipe(buffer())
        // .pipe(babel())
        // load and init sourcemaps
        // .pipe($.sourcemaps.init({loadMaps: true}))
        // .pipe($.uglify())
        // write sourcemaps
        // .pipe($.sourcemaps.write())
        .pipe(gulp.dest(`${outputDir}/js`))
})

gulp.task('default', () => {
    gulp.watch(`${inputDir}/js/*.js`, gulp.series('js'))
})