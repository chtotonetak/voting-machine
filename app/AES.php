<?php

namespace App;

class AES
{
    /**
     * Decrypt by key and iv
     *
     * @param string $data
     * @param string $key
     * @param string $iv
     * @return string
     */
    public static function decrypt(string $data, string $key, string $iv)
    {
        return openssl_decrypt($data, 'AES-128-CBC', hex2bin($key), 1, hex2bin($iv));
    }
}