<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingDictionary extends Model
{
    protected $fillable = [ 'voting_id', 'vote_description' ];
}
