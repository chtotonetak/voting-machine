<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteCase extends Model
{
    protected $fillable = [ 'user_id', 'voting_id' ];

    /**
     * Return case of voting by user id and voting id
     *
     * @param $user_id
     * @param $voting_id
     * @return mixed
     */
    public static function getByUserAndVoting($user_id, $voting_id)
    {
        return self::where([
            ['user_id', '=', $user_id],
            ['voting_id', '=', $voting_id]
        ])->first();
    }
}
