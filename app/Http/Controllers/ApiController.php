<?php

namespace App\Http\Controllers;

use App\Bulletin;
use App\CryptoModule;
use App\VoteCase;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct()
    {
        sleep(1);
    }

    public function generateBulletin(Request $request)
    {
        if (
            !empty($request->voting_id) &&
            !empty($request->user_id)
        ) {

            if (empty(VoteCase::getByUserAndVoting($request->user_id, $request->voting_id))) {
                if (
                    !empty($request->vote_id) &&
                    !empty($request->vote_uuid)
                ) {
                    $bulletin = new Bulletin($request->vote_uuid, '', $request->vote_id);
                    VoteCase::create(['user_id' => $request->user_id, 'voting_id' => $request->voting_id]);
                    return $bulletin->json();
                }
            }

        }

        return response()->json([], 403);
    }

    public function generateKeys()
    {
        return CryptoModule::generateNewKeyPair();
    }

    public function generateSecret()
    {
        return CryptoModule::generateNewKey();
    }
}
