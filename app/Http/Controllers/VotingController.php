<?php

namespace App\Http\Controllers;

use App\BlindCompatibleRSA;
use App\Http\Requests\CreateVoting;
use App\RSA;
use App\VoteCase;
use App\Voting;
use App\VotingDictionary;
use App\VotingResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VotingController extends Controller
{
    /**
     * VotingController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Output results of voting
     *
     * @param $vote_id
     * @return mixed
     */
    public function showResult($voting_id)
    {
        $voting = Voting::findOrFail($voting_id);
        $vote_variants = $voting->voteVariants;

        return view('voting.result', [
            'voting'        => $voting,
            'vote_variants' => $vote_variants
        ]);
    }

    /**
     * Show expanded form for single voting
     *
     * @param $voting_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResultDetailed($voting_id)
    {
        $voting = Voting::findOrFail($voting_id);
        return view('voting.detailed', compact('voting'));
    }

    /**
     * Download result as json file
     *
     * @param $result_id
     * @return mixed
     */
    public function downloadResult($result_id)
    {
        $result = VotingResult::findOrFail($result_id);
        $filename = $result->vote_uuid . '.json';
        Storage::disk('local')->put($filename, $result->toJson());
        return Storage::download($filename);
    }

    /**
     * Display edit form of voting
     * Display edit form of voting
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm()
    {
        return view('voting.edit');
    }

    /**
     * Create new voting and redirect to the next step
     *
     * @param CreateVoting $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CreateVoting $request)
    {
        $voting = Voting::create([
            'name'        => $request->name,
            'description' => $request->description
        ]);

        return redirect()->route('voting.create.votes.form', ['voting_id' => $voting->id]);
    }

    /**
     * Show votes creating form for selected voting
     *
     * @param $voting_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function votesForm($voting_id)
    {
        return view('voting.votes', compact('voting_id'));
    }

    /**
     * Store votes variants for single voting
     *
     * @param $voting_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeVotes($voting_id, Request $request)
    {
        $voting = Voting::findOrFail($voting_id);
        foreach ($request->vote as $vote) {
            VotingDictionary::create([
                'voting_id'        => $voting->id,
                'vote_description' => $vote
            ]);
        }

        return redirect()->route('home');
    }

    /**
     * Delete stored voting
     *
     * @param $voting_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($voting_id)
    {
        $voting = Voting::findOrFail($voting_id);
        $voting->results()->delete();
        $voting->voteVariants()->delete();
        Voting::destroy($voting_id);
        VoteCase::where('voting_id', $voting_id)->delete();

        return redirect()->back();
    }

    /**
     * Show form with single voting variants
     *
     * @param $voting_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function doVote($voting_id)
    {
        $voting = Voting::findOrFail($voting_id);
        $vote_variants = $voting->voteVariants;

        $voteCase = VoteCase::getByUserAndVoting(Auth::user()->id, $voting_id);
        if (!empty($voteCase)) app()->abort(403, __('Double vote not permitted'));

        return view('voting.vote-one', [
            'voting'        => $voting,
            'vote_variants' => $vote_variants
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function voteStepOne(Request $request)
    {
        // Get voter's public key
        $userPublicKey = Auth::user()->getUserPublicKey();
        if (empty($userPublicKey)) app()->abort(403, __('Cannot get user public key'));

        // Decrypt signature
        try {
            $decryptedHash = RSA::decryptByPublicKey($request->post('digital_signature'), $userPublicKey, OPENSSL_PKCS1_PADDING);
            if (empty($decryptedHash)) app()->abort(403, __('Unable to decipher a signature'));

            // Verify signature
            $computedHash = hash('sha256', $request->post('bulletin_blinded'));
            if (strcasecmp($decryptedHash, $computedHash) !== 0) app()->abort(403, __('Hashes is not equal'));

            // Sign bulletin
            $privateKey = file_get_contents(config('app.validator-private-key-path'));
            $signedBulletin = BlindCompatibleRSA::sign(base64_decode($request->post('bulletin_blinded')), $privateKey);

            return view('voting.vote-two', [
                'signedBulletin' => base64_encode($signedBulletin),
                'validatorPublicKey' => file_get_contents(config('app.validator-public-key-path')),
            ]);
        } catch (\Exception $e) {
            app()->abort(500);
        }

        return redirect()->route('home');
    }


    public function voteStepTwo(int $votingId, Request $request)
    {
        $aesData = json_decode($request->post('aes_data'), true);

        // Unsign by public key
        $bulletinEncrypted = BlindCompatibleRSA::unSign(base64_decode($request->post('ds_en')), $request->post('validator_public_key'));
        $bulletinEncryptedHash = hash('sha256', $bulletinEncrypted);
        $calculatedHash = hash('sha256', $aesData['ct']);
        if (strcasecmp($bulletinEncryptedHash, $calculatedHash) !== 0) app()->abort(403, __('Unable to verify signature'));

        // Decrypt by secret key
        $passphrase = $request->post('secret_key');
        try {
            $salt = hex2bin($aesData['s']);
            $iv  = hex2bin($aesData['iv']);
        } catch(\Exception $e) { app()->abort(403, __('Unable to decrypt bulletin')); }
        $ct = base64_decode($aesData['ct']);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $bulletinData = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        $bulletin = json_decode($bulletinData, true);

        // Store bulletin data
        if (!empty($voteId = $bulletin['vote_id'])) {
            // Save that user did vote
            VoteCase::updateOrCreate([
                'user_id' => Auth::user()->id,
                'voting_id' => $votingId
            ]);
            // Save result
            VotingResult::updateOrCreate([
                'voting_id' => $votingId,
                'vote_id' => $voteId,
                'bulletin_encrypted' => $aesData['ct'],
                'bulletin_decrypted' => $bulletinData,
                'vote_uuid' => Auth::user()->vote_uuid,
                'digital_sign' => $request->post('ds_en'),
                'user_secret_key' => $passphrase,
                'user_public_key' => $userPublicKey = Auth::user()->getUserPublicKey()
            ]);
        } else {
            app()->abort(403, __('Malformed bulletin received'));
        }

        return redirect()->route('home');
    }
}
