<?php

namespace App;

class RSA
{
    // Block size for encryption block cipher
    const ENCRYPT_BLOCK_SIZE = 200;

    // Block size for decryption block cipher
    const DECRYPT_BLOCK_SIZE = 256;

    /**
     * @param string $key
     * @param bool $isPrivate
     * @return bool|null|resource
     * @throws \Exception
     */
    private static function loadKey(string $key, bool $isPrivate)
    {
        $pk = null;
        if ($isPrivate) {
            $pk = openssl_get_privatekey($key);
        } else {
            $pk = openssl_get_publickey($key);
        }
        if (empty($pk)) { throw new \Exception("Unable to load RSA key"); }
        return $pk;
    }

    /**
     * @param string $data
     * @param string $key
     * @param int $padding
     * @param bool $isPrivate
     * @return string
     * @throws \Exception
     */
    private static function encryptByKey(string $data, string $key, int $padding, bool $isPrivate)
    {
        $pk = self::loadKey($key, $isPrivate);

        $data = str_split($data, self::ENCRYPT_BLOCK_SIZE);
        $encrypted = "";
        foreach ($data as $chunk) {
            $encryptedChunk = "";

            if ($isPrivate) {
                $chunkEncryptionStatus = openssl_private_encrypt($chunk, $encryptedChunk, $pk, $padding);
            } else {
                $chunkEncryptionStatus = openssl_public_encrypt($chunk, $encryptedChunk, $pk, $padding);
            }
            if (!$chunkEncryptionStatus) throw new \Exception("Too big data for encrypting");

            $encrypted .= $encryptedChunk;
        }

        return chunk_split(base64_encode($encrypted));
    }

    /**
     * @param string $base64Data
     * @param string $key
     * @param int $padding
     * @param bool $isPrivate
     * @return string
     * @throws \Exception
     */
    private static function decryptByKey(string $base64Data, string $key, int $padding, bool $isPrivate)
    {
        $pk = self::loadKey($key, $isPrivate);

        $data = str_split(base64_decode($base64Data), self::DECRYPT_BLOCK_SIZE);
        $decrypted = "";
        foreach ($data as $chunk) {
            $decryptedChunk = "";

            if ($isPrivate) {
                $chunkDecryptionStatus = openssl_private_decrypt($chunk, $decryptedChunk, $pk, $padding);
            } else {
                $chunkDecryptionStatus = openssl_public_decrypt($chunk, $decryptedChunk, $pk, $padding);
            }
            if (!$chunkDecryptionStatus) throw new \Exception("Too big data for decrypting");

            $decrypted .= $decryptedChunk;
        }

        return $decrypted;
    }

    /**
     * Encrypt string with base64-encoded private key
     *
     * @param string $data
     * @param string $key
     * @param int $padding
     * @return string
     * @throws \Exception
     */
    public static function encryptByPrivateKey(string $data, string $key, int $padding)
    {
        return self::encryptByKey($data, $key, $padding, true);
    }

    /**
     * Encrypt string with base64-encoded public key
     *
     * @param string $data
     * @param string $key
     * @param int $padding
     * @return string
     * @throws \Exception
     */
    public static function encryptByPublicKey(string $data, string $key, int $padding)
    {
        return self::encryptByKey($data, $key, $padding, false);
    }

    /**
     * Decrypt base64-encoded data string with with base64-encoded public key
     *
     * @param string $base64Data
     * @param string $key
     * @param int $padding
     * @return mixed
     * @throws \Exception
     */
    public static function decryptByPublicKey(string $base64Data, string $key, int $padding)
    {
        return self::decryptByKey($base64Data, $key, $padding, false);
    }

    /**
     * Decrypt base64-encoded data string with with base64-encoded private key
     *
     * @param string $base64Data
     * @param string $key
     * @param int $padding
     * @return mixed
     * @throws \Exception
     */
    public static function decryptByPrivateKey(string $base64Data, string $key, int $padding)
    {
        return self::decryptByKey($base64Data, $key, $padding, true);
    }

    /**
     * Return newly generated pair of RSA keys
     *
     * @param int $length
     * @return array|null
     */
    public static function generateNewKeyPair(int $length)
    {
        $config = [
            'digest_alg' => 'sha512',
            'private_key_bits' => $length,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ];

        $resource = openssl_pkey_new($config);
        if ($resource) {
            openssl_pkey_export($resource, $private_key);
            $public_key = openssl_pkey_get_details($resource);
            $public_key = $public_key['key'];

            return [
                $public_key,
                $private_key
            ];
        }

        return null;
    }
}