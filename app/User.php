<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Webpatser\Uuid\Uuid;

class User extends Authenticatable
{
    use HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'first_name', 'second_name', 'last_name', 'phone', 'vote_uuid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->vote_uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * Fetch user public key from repository
     *
     * @return null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUserPublicKey()
    {
        $client = new Client();
        $response = $client->request('GET', config('app.kcc-host') . '/user/' . Auth::user()->vote_uuid . '/key');
        $data = json_decode($response->getBody());
        if (!empty($data) && !empty($data->publicKey)) {
            return $userPublicKey = $data->publicKey;
        }
        return null;
    }
}
