<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingResult extends Model
{
    protected $fillable = ['voting_id', 'vote_id', 'bulletin_encrypted', 'bulletin_decrypted', 'vote_uuid', 'digital_sign', 'user_secret_key', 'user_public_key'];
}
