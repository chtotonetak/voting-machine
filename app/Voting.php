<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    protected $fillable = [ 'name', 'description' ];

    public function voteVariants()
    {
        return $this->hasMany(VotingDictionary::class, 'voting_id');
    }

    public function results()
    {
        return $this->hasMany(VotingResult::class, 'voting_id');
    }
}
