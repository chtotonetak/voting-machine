<?php

namespace App;

class BlindCompatibleRSA
{
    public static function sign(string $data, string $privateKey)
    {
        $rsaHelper = new \phpseclib\Crypt\RSA();
        $m = $rsaHelper->_os2ip($data);
        $keyComponents = $rsaHelper->_parseKey($privateKey, \phpseclib\Crypt\RSA::PRIVATE_FORMAT_PKCS1);
        if (!empty($keyComponents)) {
            $n = $keyComponents['modulus'];
            $d = $keyComponents['privateExponent'];
            if (!empty($n) && !empty($d) && !empty($m)) {
                $k = strlen($n->toBytes());
                $signed = $m->modPow($d, $n);
                return $rsaHelper->_i2osp($signed, $k); // String will be padded by zeros to the modulus string
            }
        }
        return null;
    }

    public static function unSign(string $data, string $publicKey)
    {
        $rsaHelper = new \phpseclib\Crypt\RSA();
        $m = $rsaHelper->_os2ip($data);
        $keyComponents = $rsaHelper->_parseKey($publicKey, \phpseclib\Crypt\RSA::PUBLIC_FORMAT_PKCS1);
        if (!empty($keyComponents)) {
            $n = $keyComponents['modulus'];
            $e = $keyComponents['publicExponent'];
            if (!empty($n) && !empty($e) && !empty($m)) {
                $k = strlen($n->toBytes());
                $unSigned = $m->modPow($e, $n);
                $unSignedString = $rsaHelper->_i2osp($unSigned, $k);
                $unSignedString = str_replace("\x00", '', $unSignedString); // Unpad string
                return $unSignedString;
            }
        }
        return null;
    }
}