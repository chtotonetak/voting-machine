FROM php:7.2-apache

# Install required extensions
RUN docker-php-ext-install pdo_mysql

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN apt-get update && apt-get install -y git unzip

# Configure Apache
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

# Copy application
COPY ./ /var/www/html

RUN composer install --no-dev

RUN chown www-data:www-data -R /var/www/html/storage/

# Add NodeJS
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt -y install nodejs

# Build JS
RUN npm install -g yarn
RUN yarn install
RUN yarn build-js