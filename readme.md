## Voting Machine based on FOO92-protocol

### For generating keys use following commands
```bash
openssl genrsa -out private.pem 512
openssl rsa -in private.pem -out public.pem -outform PEM -pubout
openssl rand -base64 128
```

[Log] B_en: {"vote_id":"6"} (step-one.js, line 4856)
[Log] R: 9970542033074719572591602958691803981068401446201085797085708096684221905557293208674651597631298532597124370731324287513186831458010683849356744694319528 (step-one.js, line 4859)
[Log] B_bl: UKfqQAMVnxeEnNOVF3xSrI12y7HRCnXu/X4vskAYDi28K/qGP24chXIBgHk1WxlLQ8pWK2JvjdQ0qP56SLs3dA== (step-one.js, line 4861)
[Log] h(B_bl): 92bb82b210d87ba3a6b6bfd40496b322c895971f7dccdeb99080fe6b5378cb85 (step-one.js, line 4862)
[Log] DS_v(B_bl): UtcDZtWUId5U0GEX/+d8d8+VvGhh3NN3pmjPl4VVIuz7S2BsYXK73wn2WoKs2BkfWIesNKH0lzphx+y8F1XaryA5IaZeSnNlzNogldZKgqhkQ7CnVqjg3nSIXuNfdOVyVSLDUjrExU4iSrqUkKAurJIoxVWiLpUCCvhLBXc1m8A= (step-one.js, line 4862)
[Log] base64(B_bl_signed): p4isF0KQmwYwuMomKcO+fo/3c0hlI5kyQhFgZgQfD9pvi7nql7FxC2bKIyHZ4JqXZDyx74F1oRm4 (step-two.js, line 4326)
Rto4eQy3LQ==

[Log] base64(B_en_signed): MvdY+E6UVpYqwYz7BlVU0rnCPAbX5rE6+hAAJ2Vg7UhsbLCT4VHPC6dKLaHAPePFAQuc4w1ATrmwwJ71UiiFWw== (step-two.js, line 4327)
[Log] Public exp: 65537 (step-two.js, line 4328)
[Log] Public modulus: 10540006273691506842204230315458585675781502271031105956288414072638573148129111030745489658337037236612364541749490728977874873894860329509846792588759593 (step-two.js, line 4328)
[Log] B_en: #HELLO# (step-two.js, line 4328)

