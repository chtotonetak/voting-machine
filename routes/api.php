<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/generateBulletin', 'ApiController@generateBulletin')->name('api.generate.bulletin');
Route::get('/generateKeys', 'ApiController@generateKeys')->name('api.generate.keys');
Route::get('/generateSecret', 'ApiController@generateSecret')->name('api.generate.secret');

Route::get('/encryptByKey', 'ApiContoller@encryptByKey')->name('api.encrypt_by_key');
