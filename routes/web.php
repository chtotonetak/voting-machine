<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('/voting')->group(function () {

    Route::get('/result/{voting_id}', 'VotingController@showResult')->name('voting.result');
    Route::get('/result/{voting_id}/detailed', 'VotingController@showResultDetailed')->name('voting.result.detailed');
    Route::get('/result/{result_id}/download', 'VotingController@downloadResult')->name('voting.result.download');
    Route::get('/vote/{voting_id}', 'VotingController@doVote')->name('voting.vote');
    Route::post('/vote/{voting_id}', 'VotingController@voteStepOne')->name('voting.vote.do');
    Route::post('/vote/{voting_id}/send', 'VotingController@voteStepTwo')->name('voting.vote.send');

    Route::group(['middleware' => ['admin']], function () {

        Route::prefix('/create')->group(function () {
            Route::get('/', 'VotingController@createForm')->name('voting.create.form');
            Route::post('/', 'VotingController@create')->name('voting.create');
            Route::get('/{voting_id}/votes', 'VotingController@votesForm')->name('voting.create.votes.form');
            Route::post('/{voting_id}/votes', 'VotingController@storeVotes')->name('voting.create.votes');
        });

        Route::prefix('/delete')->group(function () {
            Route::get('/voting/{voting_id}', 'VotingController@delete')->name('voting.delete');
        });

    });
});

Route::get('/', 'HomeController@index')->name('home');
