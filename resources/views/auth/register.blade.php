@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-one-quarter">
                <p class="title is-4 has-text-centered">{{ __('Register') }}</p>
                <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="field">
                        <label for="email" class="label">{{ __('E-Mail') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('email') ? ' is-danger' : '' }}" type="email" id="email"
                                   name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="password" class="label">{{ __('Password') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('password') ? ' is-danger' : '' }}" type="password" id="password"
                                   name="password" placeholder="{{ __('Password') }}" required>
                        </div>
                        @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="password-confirmation" class="label">{{ __('Password confirmation') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('password') ? ' is-danger' : '' }}" type="password" id="password-confirmation"
                                   name="password_confirmation" placeholder="{{ __('Password') }}" required>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="first-name" class="label">{{ __('Name') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('first_name') ? ' is-danger' : '' }}" type="text" id="first-name"
                                   name="first_name" placeholder="{{ __('Name') }}" value="{{ old('first_name') }}" required>
                        </div>
                        @if ($errors->has('first_name'))
                            <p class="help is-danger">{{ $errors->first('first_name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="second-name" class="label">{{ __('Second name') }} ({{ __('optionally') }})</label>
                        <div class="control">
                            <input class="input{{ $errors->has('second_name') ? ' is-danger' : '' }}" type="text" id="second-name"
                                   name="second_name" placeholder="{{ __('Second name') }}" value="{{ old('second_name') }}">
                        </div>
                        @if ($errors->has('second_name'))
                            <p class="help is-danger">{{ $errors->first('second_name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="last-name" class="label">{{ __('Last name') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('last_name') ? ' is-danger' : '' }}" type="text" id="last-name"
                                   name="last_name" placeholder="{{ __('Last name') }}" value="{{ old('last_name') }}" required>
                        </div>
                        @if ($errors->has('last_name'))
                            <p class="help is-danger">{{ $errors->first('last_name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="phone-number" class="label">{{ __('Phone number') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('phone') ? ' is-danger' : '' }}" type="text" id="phone-number"
                                   name="phone" placeholder="{{ __('Phone number') }}" value="{{ old('phone') }}" required>
                        </div>
                        @if ($errors->has('phone'))
                            <p class="help is-danger">{{ $errors->first('phone') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="public-key" class="label">
                            <a href="http://travistidwell.com/jsencrypt/demo/" target="_blank">{{ __('RSA public key') }}</a>
                        </label>

                        <div class="file has-name is-fullwidth">
                            <label class="file-label">
                                <input class="file-input" type="file" name="public-key" id="public-key">
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        {{ __('Choose a file…') }}
                                    </span>
                                </span>
                                <span class="file-name" id="file-name"></span>
                            </label>
                        </div>

                        @if ($errors->has('public-key'))
                            <p class="help is-danger">{{ $errors->first('public-key') }}</p>
                        @endif

                        <p>
                            <small>{{ __('Please, pay attention! You cannot change it later.') }}</small>
                        </p>
                    </div>

                    <button type="submit" class="button is-info is-outlined is-pulled-right">
                        {{ __('Register') }}
                    </button>

                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#phone-number').mask('8 (000) 000-00-00')

            var file = document.getElementById('public-key');
            file.onchange = function () {
                if (file.files.length > 0) {
                    document.getElementById('file-name').innerHTML = file.files[0].name;
                }
            };
        })
    </script>
@endsection
