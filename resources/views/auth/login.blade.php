@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="columns is-centered">
            <div class="column is-one-quarter">
                <p class="title is-4 has-text-centered">{{ __('Sign In') }}</p>
                <form action="{{ route('login') }}" method="post">
                    @csrf

                    <div class="field">
                        <label for="email" class="label">{{ __('E-Mail') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('email') ? ' is-danger' : '' }}" type="email" id="email"
                                   name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="password" class="label">{{ __('Password') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('password') ? ' is-danger' : '' }}" type="password" id="password"
                                   name="password" placeholder="{{ __('Password') }}" required autofocus>
                        </div>
                        @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>

                    <button type="submit" class="button is-info is-pulled-right is-outlined">
                        {{ __('Login') }}
                    </button>

                </form>
            </div>
        </div>

    </div>
@endsection
