@extends('layouts.app')

@section('content')
    <p class="title is-4 has-text-centered">{{ __('Create voting') }}</p>

    <div class="container">
        <div class="columns is-centered">
            <div class="column is-one-quarter">
                <form action="{{ route('voting.create') }}" method="post">
                    @csrf

                    <div class="field">
                        <label for="name" class="label">{{ __('Voting name') }}</label>
                        <div class="control">
                            <input class="input{{ $errors->has('name') ? ' is-danger' : '' }}" type="text" id="name"
                                   name="name" placeholder="{{ __('Voting name') }}" value="{{ old('name') }}" required autofocus>
                        </div>
                        @if ($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label for="description" class="label">{{ __('Description') }}</label>
                        <div class="control">
                            <textarea class="textarea{{ $errors->has('description') ? ' is-danger' : '' }}" type="text" id="description"
                                      name="description" placeholder="{{ __('Description') }}" rows="5"></textarea>
                        </div>
                        @if ($errors->has('description'))
                            <p class="help is-danger">{{ $errors->first('description') }}</p>
                        @endif
                    </div>

                    <button type="submit" class="button is-pulled-right">
                        {{ __('Next step') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
