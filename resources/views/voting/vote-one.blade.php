@extends('layouts.app')

@section('content')

    <br>

    <div class="container">
        <div class="columns is-centered">
            <div class="column is-4-desktop has-text-centered">
                <p class="title is-4">{{ $voting->name }}</p>
                <form id="vote-form">
                    @forelse($vote_variants as $vote_variant)
                        <div class="control">
                            <label class="radio">
                                <input type="radio" value="{{ $vote_variant->id }}" name="vote_id">
                                &nbsp;{{ $vote_variant->vote_description }}
                            </label>
                        </div>
                    @empty
                       <p>{{ __('Votes not found') }}</p>
                    @endforelse

                    <input type="hidden" name="voting_id" value="{{ $voting->id }}">
                    <input type="hidden" name="vote_uuid" value="{{ Auth::user()->vote_uuid }}">

                    <div class="field">
                        <label for="aes-key" class="label">
                            <a href="http://www.allkeysgenerator.com/Random/Security-Encryption-Key-Generator.aspx" target="_blank">{{ __('Encryption key') }}</a>
                        </label>
                        <div class="control">
                            <textarea class="textarea" id="aes-key" rows="3" placeholder="{{ __('Encryption key') }}" required></textarea>
                        </div>
                        <p class="has-text-left">
                            <small>
                                {{ __('Your selection will be cyphered with your encrypted key. Be sure you saved this key in safe place. Without this key system cannot approve your vote.') }}
                            </small>
                        </p>
                    </div>

                    <button id="do-vote" class="button is-info is-outlined">{{ __('Do vote') }}</button>
                </form>
            </div>
        </div>
    </div>

    <div id="blinder-form" class="container is-hidden">
        <div class="columns is-centered">
            <div class="column is-4-desktop has-text-centered">
                <form id="blind-form">
                    <div class="field">
                        <p class="has-text-left">
                            <small>{{ __('It\'s required your RSA key pair to signature your encrypted bulletin. Blinder factor will be computed.') }}</small>
                        </p>
                    </div>

                    <div class="field">
                        <label for="public-key" class="label">
                            {{ __('RSA public key') }}
                        </label>

                        <div class="file has-name is-fullwidth">
                            <label class="file-label">
                                <input class="file-input" type="file" name="public-key" id="public-key">
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        {{ __('Choose a file…') }}
                                    </span>
                                </span>
                                <span class="file-name"></span>
                            </label>
                        </div>
                    </div>

                    <div class="field">
                        <label for="private-key" class="label">
                            {{ __('RSA private key') }}
                        </label>

                        <div class="file has-name is-fullwidth">
                            <label class="file-label">
                                <input class="file-input" type="file" name="private-key" id="private-key">
                                <span class="file-cta">
                                    <span class="file-icon">
                                        <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        {{ __('Choose a file…') }}
                                    </span>
                                </span>
                                <span class="file-name"></span>
                            </label>
                        </div>
                    </div>

                    <button id="do-blind" class="button is-info is-outlined" disabled>{{ __('Sign') }}</button>
                </form>
            </div>
        </div>
    </div>

    <div class="is-hidden">
        <form action="{{ route('voting.vote.do', ['voting_id' => Request::route('voting_id')]) }}" id="step-one-form" method="post">
            @csrf
            <input type="hidden" name="bulletin_blinded">
            <input type="hidden" name="digital_signature">
        </form>
    </div>

    <script>
        window.KCC_HOST = '{{ config('app.kcc-host') }}';
        window.VALIDATOR_ID = '{{ config('app.validator-uuid') }}';
    </script>
    <script src="{{ asset('js/step-one.js?' . date('U')) }}"></script>
@endsection
