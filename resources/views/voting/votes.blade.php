@extends('layouts.app')

@section('content')
    <p class="title is-4 has-text-centered">{{ __('Create voting variants') }}</p>

    <div class="columns is-centered">
        <div class="column is-one-quarter">
            <form action="{{ route('voting.create.votes', ['voting_id' => $voting_id]) }}" method="post">
                @csrf

                <div id="fields-area">
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" placeholder="{{ __('Description') }}"
                                   name="vote[]" required autofocus>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <input class="input" type="text" placeholder="{{ __('Description') }}"
                                   name="vote[]" required>
                        </div>
                    </div>
                </div>

                <br>

                <div class="buttons is-pulled-right">
                    <a id="add-field" class="button">{{ __('Add more') }}</a>
                    <button type="submit" class="button is-info is-outlined">{{ __('Finish creating') }}</button>
                </div>

            </form>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#add-field').click(function (event) {
                event.preventDefault()
                $('#fields-area').append('<div class="field">\n' +
                    '                        <div class="control">\n' +
                    '                            <input class="input" type="text" placeholder="{{ __("Description") }}"\n' +
                    '                                   name="vote[]" required>\n' +
                    '                        </div>\n' +
                    '                    </div>')
            })
        })
    </script>
@endsection
