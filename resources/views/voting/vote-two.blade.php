@extends('layouts.app')

@section('content')

    <br>

    <div class="container">
        <div class="columns is-centered">
            <div class="column is-4-desktop has-text-centered">
                <p class="title is-4">{{ __('Last step') }}</p>
                <form id="vote-form" method="post" action="{{ route('voting.vote.send', ['voting_id' => Request::route('voting_id')]) }}">
                    @csrf

                    <div class="field">
                        <label for="aes-key" class="label">
                            {{ __('Encryption key') }}
                        </label>
                        <div class="control">
                            <textarea class="textarea" id="aes-key" rows="3" placeholder="{{ __('Encryption key') }}" name="secret_key" required></textarea>
                        </div>
                        <p class="has-text-left">
                            <small>
                                {{ __('Please enter your secret key again. This key was submitted automatically.') }}
                            </small>
                        </p>
                    </div>

                    <div class="field">
                        <label for="aes-key" class="label">
                            {{ __('Unique label') }}
                        </label>
                        <div class="control">
                            <input class="input" disabled value="{{ \Illuminate\Support\Facades\Auth::user()->vote_uuid }}">
                        </div>
                        <p class="has-text-left">
                            <small>
                                {{ __('Copy your unique label please.') }}
                            </small>
                        </p>
                    </div>

                    <input type="hidden" name="bulletin_blinded_signed" value="{{ $signedBulletin }}">
                    <input type="hidden" name="validator_public_key" value="{{ $validatorPublicKey }}">

                    <input type="hidden" name="b_en">
                    <input type="hidden" name="ds_en">
                    <input type="hidden" name="aes_data">

                    <button id="do-vote" class="button is-info is-outlined">{{ __('Do vote') }}</button>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/step-two.js?' . date('U')) }}"></script>
@endsection
