@extends('layouts.app')

@section('content')
    <p class="title is-4 has-text-centered">{{ __('Results of voting #') }}{{ $voting->id }}</p>

    <div class="buttons is-centered">
        <a href="{{ route('voting.result.detailed', ['voting_id' => $voting->id]) }}" class="button">{{ __('Show details') }}</a>
    </div>

    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <table class="table is-fullwidth is-striped">
                    <thead>
                        <tr>
                            <th>{{ __('Variant') }}</th>
                            <th>{{ __('Count of votes') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($vote_variants as $vote_variant)
                            <tr>
                                <td>{{ $vote_variant->vote_description }}</td>
                                <td>{{ $voting->results()->where('vote_id', $vote_variant->id)->count() }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">{{ __('Votes not found') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
