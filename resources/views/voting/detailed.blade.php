@extends('layouts.app')

@section('content')
    <p class="title is-4 has-text-centered">{{ __('Results of voting #') }}{{ $voting->id }}</p>

    <div class="container">
        <div class="columns is-centered">
            <div class="column">
                <table class="table is-fullwidth is-striped">
                    <thead>
                        <tr>
                            <th>{{ __('Variant') }}</th>
                            <th>{{ __('UUID') }}</th>
                            <th>{{ __('Download') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($voting->results as $result)
                            <tr>
                                <td>{{ \App\VotingDictionary::find($result->vote_id)->vote_description }}</td>
                                <td>{{ $result->vote_uuid }}</td>
                                <td><a href="{{ route('voting.result.download', ['result_id' => $result->id]) }}">{{ __('Download') }}</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" style="text-align: center;">{{ __('Votes not found') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
