<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/bulma.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <div class="container">
            <div class="level">
                <div class="level-left">
                    <div class="column">
                        <a href="{{ url('/') }}">
                            <h2 class="title">{{ config('app.name') }}</h2>
                        </a>
                    </div>
                </div>
                <div class="level-right">
                    <!-- Authentication Links -->
                    @guest
                        <div class="column">
                            <div class="buttons">
                                <a class="button" href="{{ route('login') }}">{{ __('Login') }}</a>
                                @if (Route::has('register'))
                                    <a class="button" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="column">
                            <p>
                                {{ __('auth.signed_in', ['last_name' => Auth::user()->last_name, 'first_name' => Auth::user()->first_name]) }},
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ mb_strtolower(__('Logout')) }}.
                                </a>
                            </p>
                            <!-- Logout form -->
                            <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    @endguest
                </div>
            </div>
        </div>

        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
