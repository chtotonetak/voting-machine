@extends('layouts.app')

@section('content')
<div class="container">

    <p class="title is-4 has-text-centered">{{ __('Voting centre') }}</p>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @role('admin')
        <div class="column">
            <div class="buttons is-centered">
                <a href="{{ route('voting.create.form') }}" class="button">{{ __('Create voting') }}</a>
            </div>
        </div>
    @endrole

    <br>

    <table class="table is-fullwidth" style="table-layout: fixed;">
        <thead>
            <tr>
                <th>{{ __('Voting #') }}</th>
                <th>{{ __('Voting name') }}</th>
                <th>{{ __('Voters count') }}</th>
                <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse(\App\Voting::withCount('results')->get() as $voting)
            <tr>
                <td>{{ $voting->id }}</td>
                <td>{{ $voting->name }}</td>
                <td>{{ $voting->results_count }}</td>
                <td>
                    <div class="buttons">
                        @role('admin')
                            <a class="button is-danger is-outlined" href="{{ route('voting.delete', ['voting_id' => $voting->id]) }}">{{ __('Delete') }}</a>
                        @else
                            <a class="button is-info is-outlined" href="{{ route('voting.vote', ['voting_id' => $voting->id]) }}">{{ __('Do vote') }}</a>
                        @endrole
                        <a class="button is-success is-outlined" href="{{ route('voting.result', ['voting_id' => $voting->id]) }}">{{ __('Results') }}</a>
                    </div>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="5" style="text-align: center">{{ __('No existed votings') }}</td>
                </tr>
            @endforelse
        </tbody>
    </table>

</div>
@endsection
