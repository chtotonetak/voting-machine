const BigInteger = require('jsbn').BigInteger;
const Cookies = require('js-cookie');
const NodeRSA = require('node-rsa');
const $ = require('jquery');

$(document).ready(() => {
    // Retrieve signed bulletin
    const signedBulletinBase64 = $('input[name=bulletin_blinded_signed]').val();
    // const signedBulletinBase64 = "aW//LVRkYr+HYjEEKRqdZAKHTh0cOIv9XTOVmFMheH/oFjiczoj/rWvP3TJGbBIq7F2YBNFCL36JRV5UzwkR0Q==";
    const signedBulletin = Buffer.from(signedBulletinBase64, 'base64');

    console.log('base64(B_bl_signed): ' + signedBulletinBase64);

    // Unblind
    const key = new NodeRSA($('input[name=validator_public_key]').val());
    const r = new BigInteger(Cookies.get('blinder_factor'));
    const N = key.keyPair.n;
    const signed = new BigInteger(signedBulletin);
    const unblinded = signed.multiply(r.modInverse(N)).mod(N);
    const unblindedBase64 = Buffer.from(unblinded.toByteArray()).toString('base64');

    console.log('base64(B_en_signed): ' + unblindedBase64);
    console.log('B_en_signed: ' + atob(unblindedBase64));

    // Set form data
    const b_en = Cookies.get('b_en');
    const aes_data = Cookies.get('k_secr');

    $('input[name=b_en]').val(b_en);
    $('input[name=ds_en]').val(unblindedBase64);
    $('input[name=aes_data]').val(aes_data);
});