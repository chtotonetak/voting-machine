const $ = require('jquery');
const NodeRSA = require('node-rsa');
const axios = require('axios');
const BigInteger = require('jsbn').BigInteger;
const sha256 = require('js-sha256');
const Cookies = require('js-cookie');
const CryptoJS = require('crypto-js');

const vote_form_selector = '#vote-form';

let voterKeyPair = {
    public: null,
    private: null
}
let voterBlinderFactor = null;
let validatorKey = null;

let bulletinEn = null;
let bulletinBl = null;
let bulletinDS = null;

/**
 * AES JSON formatter for CryptoJS
 *
 * @author BrainFooLong (bfldev.com)
 * @link https://github.com/brainfoolong/cryptojs-aes-php
 */

var CryptoJSAesJson = {
    stringify: function (cipherParams) {
        var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
        if (cipherParams.iv) j.iv = cipherParams.iv.toString();
        if (cipherParams.salt) j.s = cipherParams.salt.toString();
        return JSON.stringify(j).replace(/\s/g, '');
    },
    parse: function (jsonStr) {
        var j = JSON.parse(jsonStr);
        var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
        if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
        if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
        return cipherParams;
    }
}

$(document).ready(() => {
    // File input handlers
    $('input[type=file]').change(function () {
        if (this.files.length > 0) {
            $(this).siblings('.file-name').html(this.files[0].name);
        }
    });
    // Check first radio
    $(vote_form_selector + ' .control:first-child input[name=vote_id]').prop('checked', true);

    // Vote form submit trigger
    $('#vote-form').submit(e => {
        e.preventDefault();
        // Disable radio
        $(vote_form_selector + ' .control input[name=vote_id]').prop('disabled', true);
        // Check key
        const encryptionKey = $('#aes-key').val();
        if (!encryptionKey.length) {
            alert('Необходим ключ шифрования!')
            return;
        }
        // Generate bulletin
        const bulletin = generateBulletin();
        // Encryption bulletin
        bulletinEn = encryptBulletin(bulletin, encryptionKey);
        console.log('B_en: ' + bulletinEn);

        if (bulletinEn) {
            // Enable blinding part
            $('#blinder-form').removeClass('is-hidden');
            fetchValidatorPublicKey();
            waitForKeysUpload();
        }
    });

    // Blinder form submit trigger
    $('#blinder-form').submit(e => {
        e.preventDefault();
        $('#do-blind').prop('disabled', true);

        if (validatorKey) {
            blindBulletin();
            // Calculate signature
            const blindedHash = sha256(bulletinBl);
            console.log('h(B_bl): ' + blindedHash);
            voterKeyPair.private.setOptions({
                encryptionScheme: 'pkcs1'
            });
            const signature = voterKeyPair.private.encryptPrivate(Buffer.from(blindedHash));
            bulletinDS = signature.toString('base64');
            console.log('DS_v(B_bl): ' + bulletinDS);
            // Fill and send form
            goToTheNextStep();
        }
    });
});

const blindBulletin = () => {
    validatorKey = new NodeRSA(validatorKey);

    // Calculate blinding factor
    const N = validatorKey.keyPair.n;
    const E = new BigInteger(validatorKey.keyPair.e.toString());
    const bigOne = BigInteger.ONE;

    let gcd; let r;
    do {
        r = new BigInteger(secureRandom(3)).mod(N);
        gcd = r.gcd(N);
    } while (
        !gcd.equals(bigOne) ||
        r.compareTo(N) >= 0 ||
        r.compareTo(bigOne) <= 0
    );

    voterBlinderFactor = r;
    console.log('R: ' + voterBlinderFactor);

    Cookies.set('blinder_factor', voterBlinderFactor.toString());

    // Blind bulletin
    const message = Buffer.from(bulletinEn);
    const blinded = new BigInteger(message).multiply(r.modPow(E, N)).mod(N);
    bulletinBl = Buffer.from(blinded.toByteArray()).toString('base64');
    console.log('B_bl: ' + bulletinBl);
}

const setVoteFormDisabled = isDisabled => {
    // Key textarea
    $('#aes-key').attr('disabled', isDisabled);
    // Button
    $('#do-vote').prop('disabled', isDisabled);
}

const generateBulletin = () => {
    const selection = $('input[name=vote_id]:checked', vote_form_selector).val();
    const bulletin = {'vote_id': selection};
    return JSON.stringify(bulletin);
}

const encryptBulletin = (bulletin, encryptionKey) => {
    setVoteFormDisabled(true);

    try {
        const encrypted = CryptoJS.AES.encrypt(bulletin, encryptionKey, {format: CryptoJSAesJson});

        console.log('key: ' + encrypted.key);
        console.log('iv: ' + encrypted.iv);

        Cookies.set('k_secr', encrypted.toString());
        Cookies.set('b_en', encrypted.ciphertext.toString(CryptoJS.enc.Base64));

        return encrypted.ciphertext.toString(CryptoJS.enc.Base64);
    } catch (e) {
        alert('Проверьте длину ключа! Требуется 128 бит.');
        setVoteFormDisabled(false);
    }

    return null;
}

const waitForKeysUpload = () => {
    $('input[type=file]').change(function () {
        if (this.files.length > 0) {
            const fileReader = new FileReader();
            fileReader.onload = () => {
                if (fileReader.result) {
                    if ($(this).attr('id') === 'public-key') {
                        voterKeyPair.public = new NodeRSA(fileReader.result);
                    } else {
                        voterKeyPair.private = new NodeRSA(fileReader.result);
                    }
                }

                if (voterKeyPair.public && voterKeyPair.private) {
                    $('#do-blind').prop('disabled', false);
                }
            }
            fileReader.readAsText(this.files[0]);
        }
    });
}

const fetchValidatorPublicKey = () => {
    if (KCC_HOST && VALIDATOR_ID) {
        axios.get(KCC_HOST + '/validator/' + VALIDATOR_ID + '/key')
            .then(response => {
                if (response.data.publicKey) {
                    validatorKey = response.data.publicKey;
                }
            })
            .catch(error => {
                console.log(error);
                abortProtocol('Невозможно проверить регистратора.')
            });
    } else {
        abortProtocol('Данные регистратора не переданы.')
    }
}

const abortProtocol = error => {
    alert(error || 'Произошла непредвиденная ошибка, продолжение невозможно.');
}

/**
 * Simple wrapper for window.crypto.getRandomValues
 * @param {Number} size Number of bytes
 * @returns {Uint8Array} Contains random bytes
 * @author Benji Altman
 */
const secureRandom = size => {
    let tmp = new Uint8Array(size);
    window.crypto.getRandomValues(tmp);
    return tmp;
}

const goToTheNextStep = () => {
    $('#step-one-form input[name=bulletin_blinded]').val(bulletinBl);
    $('#step-one-form input[name=digital_signature]').val(bulletinDS);
    $('#step-one-form').submit();
}