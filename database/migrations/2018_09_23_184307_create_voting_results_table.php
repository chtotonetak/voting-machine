<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voting_id');
            $table->unsignedInteger('vote_id');
            $table->string('bulletin_encrypted');
            $table->string('bulletin_decrypted');
            $table->string('vote_uuid');
            $table->string('digital_sign');
            $table->string('user_secret_key');
            $table->text('user_public_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_results');
    }
}
