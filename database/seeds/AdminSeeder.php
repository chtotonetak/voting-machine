<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    public function run()
    {
        // Creating administrative role
        Role::create(['name' => 'admin']);
        // Creating admin user
        $user = User::create([
            'first_name' => 'Регистратор',
            'last_name'  => 'Голосования',
            'email'      => 'admin@chtoto.net',
            'password'   => Hash::make('qwaszx'),
            'phone'      => '8 (777) 777-77-77'
        ]);
        // Assign role to user
        $user->assignRole('admin');
    }
}
