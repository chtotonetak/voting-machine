<?php

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Seeder;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ValidatorKeySeeder extends Seeder
{
    /**
     * Store validator key in Key Certification Centre
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        try {
            $client->request('PUT', config('app.kcc-host') . '/validator/' . config('app.validator-uuid') . '/key',
                [
                    'json' => [
                        'publicKey' => file_get_contents(config('app.validator-public-key-path'))
                    ]
                ]);
        } catch (GuzzleException $ex) {
            Log::error($ex->getMessage());
        }
    }
}
